import { shallowMount } from "@vue/test-utils";
import Button from "../../src/components/Button/Button.vue";
import Spin from "../../src/components/Spin/Spin.vue";

describe("button", () => {
  let wrapper = null;

  const BUTTON_TEXT = "Click me";

  const createComponent = props => {
    wrapper = shallowMount(Button, {
      slots: {
        default: BUTTON_TEXT
      },
      propsData: props
    });
  };

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
      wrapper = null;
    }
  });

  it("renders properly", () => {
    createComponent();
    const classList = wrapper.classes();

    expect(wrapper.element.tagName).toBe("BUTTON");
    expect(wrapper.text()).toBe(BUTTON_TEXT);
    expect(wrapper.attributes("type")).toBe("button");
    expect(classList).toContain("dv-button");
    expect(classList).toContain("dv-button--normal");
    expect(classList).toContain("dv-button--basic");
  });

  it("renders link when `href` is provided", () => {
    createComponent({ href: "/#" });
    const classList = wrapper.classes();

    expect(wrapper.element.tagName).toBe("A");
    expect(wrapper.text()).toBe(BUTTON_TEXT);
    expect(wrapper.attributes("href")).toBe("/#");
    expect(classList).toContain("dv-button");
    expect(classList).toContain("dv-button--normal");
    expect(classList).toContain("dv-button--basic");
    expect(classList).toContain("dv-button--link");
  });

  it("indicates pending state", async () => {
    const getSpin = () => wrapper.findComponent(Spin);
    createComponent();

    expect(getSpin().exists()).toBe(false);

    wrapper.setProps({ pending: true });
    await wrapper.vm.$nextTick();

    expect(getSpin().exists()).toBe(true);

    wrapper.setProps({ pending: false });
    await wrapper.vm.$nextTick();

    expect(getSpin().exists()).toBe(false);
  });

  // it('renders element with proper css classes depending on props', () => {
  //   expect(true).toBe(false);
  // });
});
