import { shallowMount } from "@vue/test-utils";
import Spin from "../../src/components/Spin/Spin.vue";

describe("Spin", () => {
  let wrapper = null;

  const createComponent = props => {
    wrapper = shallowMount(Spin, {
      propsData: props
    });
  };

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
      wrapper = null;
    }
  });

  it('renders properly', () => {
    createComponent();
    const classList = wrapper.classes();

    expect(classList).toContain('dv-spin');
    expect(classList).toContain('dv-spin--primary');
    expect(classList).toContain('dv-spin--sm');
  });

  //TODO: test all class bindings with `each`
});
