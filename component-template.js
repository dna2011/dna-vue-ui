function component(
  renderedUsage, // props, events, methods and slots documentation rendered
  doc, // the object returned by vue-docgen-api
) {
  const { displayName, description, docsBlocks } = doc;
  return `
  # ${displayName}

  ${description ? `> ${description}` : ''}

  <component-demo
    props-stringified="${JSON.stringify(doc.props).replace(/"/g, '%042').replace(/'/g, '')}"
    slots-stringified="${typeof doc.slots === 'undefined' ? '' : JSON.stringify(doc.slots).replace(/"/g, '%042').replace(/'/g, '')}"
    component-name="dv-${displayName.split(' ').join('-').toLowerCase()}">
  </component-demo>

  ${docsBlocks ? `---\n${docsBlocks.join('\n---\n')}` : ''}
  ${renderedUsage.props}
  ${renderedUsage.methods}
  ${renderedUsage.events}
  ${renderedUsage.slots}
  `;
}

module.exports = component;
