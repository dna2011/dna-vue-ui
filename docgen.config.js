require('dotenv').config();
const path = require('path');
const fs = require('fs');
const componentTemplate = require('./component-template');

function getComponentsNames() {
  const componentsToDocument = [];
  fs.readdirSync(path.join(__dirname, 'src/components'), { withFileTypes: true })
    .forEach((componentDir) => {
      if (componentDir.isDirectory()) {
        componentsToDocument.push(componentDir.name);
      }
    });

  return componentsToDocument;
}

function getGlobExpression() {
  const componentsNames = getComponentsNames();
  const globStart = '**/{';
  const globEnd = '}.{js,vue}';

  const componentsSources = componentsNames.map((name) => `${name}/${name}`);
  return `${globStart}${componentsSources.join(',')}${globEnd}`;
}

module.exports = {
  componentsRoot: 'src/components',
  components: getGlobExpression(),
  outDir: `docs/${process.env.COMPONENTS_DOCUMENTATION_DIRECTORY}`,
  getDestFile: (file, config) => path.join(config.outDir, file.split('/')[0], 'README.md'),
  templates: {
    component: componentTemplate,
  },
};
