require('dotenv').config();
const fs = require('fs');
const path = require('path');

const componentsDocPath = [];
const componentsToRegister = [];
fs.readdirSync(path.resolve(__dirname, `../${process.env.COMPONENTS_DOCUMENTATION_DIRECTORY}`), { withFileTypes: true })
  .forEach(entity => {
    if (entity.isDirectory()) {
      componentsDocPath.push(`${process.env.COMPONENTS_DOCUMENTATION_DIRECTORY}/${entity.name}/`);
      componentsToRegister.push({
        name: `dv-${entity.name.toLowerCase()}`,
        path: path.resolve(__dirname, `../../src/components/${entity.name}/${entity.name}`)
      })
    }
  });

module.exports = {
  title: 'DNA UI kit',
  description: 'Homemade Vue UI components',
  themeConfig: {
    sidebar: ['/', {
      title: 'Components',
      path: `/${process.env.COMPONENTS_DOCUMENTATION_DIRECTORY}/`,
      collapsable: false,
      children: componentsDocPath
    }]
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, '../../src')
      }
    }
  }
};
