import DVPlugin from '@/components';

export default ({ Vue }) => {
  Vue.use(DVPlugin);
}