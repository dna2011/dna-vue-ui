# Icon

> Components that can draw icons, is uses mdi-font:

<component-demo
    props-stringified="[{%042name%042:%042name%042,%042description%042:%042Select md icon by name%042,%042tags%042:{%042example%042:[{%042description%042:%042account creates mdi-account icon%042,%042title%042:%042example%042}]},%042type%042:{%042name%042:%042string%042},%042required%042:true,%042defaultValue%042:{%042func%042:false,%042value%042:%042check%042}},{%042name%042:%042size%042,%042type%042:{%042name%042:%042string%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042normal%042},%042values%042:[%042small%042,%042normal%042,%042large%042,%042xlarge%042]}]"
    slots-stringified=""
    component-name="dv-icon">
</component-demo>

---

## Props

| Prop name | Description            | Type   | Values                               | Default  |
| --------- | ---------------------- | ------ | ------------------------------------ | -------- |
| name      | Select md icon by name | string | -                                    | 'check'  |
| size      |                        | string | `small`, `normal`, `large`, `xlarge` | 'normal' |
