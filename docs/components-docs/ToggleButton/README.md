# Toggle Button

<component-demo
    props-stringified="[{%042name%042:%042v-model%042,%042description%042:%042Stores toggle-button/toggle-button group state.%042,%042tags%042:{%042model%042:[{%042description%042:true,%042title%042:%042model%042}]},%042type%042:{%042name%042:%042array|boolean%042},%042required%042:true},{%042name%042:%042option%042,%042description%042:%042Specify toggle-button value. Use it when toggle-button group is presented.%042,%042type%042:{%042name%042:%042string%042}},{%042name%042:%042disabled%042,%042type%042:{%042name%042:%042boolean%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042false%042}},{%042name%042:%042acceptText%042,%042type%042:{%042name%042:%042string%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042I accept%042}},{%042name%042:%042declineText%042,%042type%042:{%042name%042:%042string%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042I decline%042}},{%042name%042:%042acceptIcon%042,%042type%042:{%042name%042:%042string%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042check%042}},{%042name%042:%042declineIcon%042,%042type%042:{%042name%042:%042string%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042close%042}},{%042name%042:%042iconic%042,%042type%042:{%042name%042:%042boolean%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042false%042}}]"
    slots-stringified=""
    component-name="dv-toggle-button">
</component-demo>

---

## Props

| Prop name   | Description                                                                | Type           | Values | Default     |
| ----------- | -------------------------------------------------------------------------- | -------------- | ------ | ----------- |
| v-model     | Stores toggle-button/toggle-button group state.                            | array\|boolean | -      |             |
| option      | Specify toggle-button value. Use it when toggle-button group is presented. | string         | -      |             |
| disabled    |                                                                            | boolean        | -      | false       |
| acceptText  |                                                                            | string         | -      | 'I accept'  |
| declineText |                                                                            | string         | -      | 'I decline' |
| acceptIcon  |                                                                            | string         | -      | 'check'     |
| declineIcon |                                                                            | string         | -      | 'close'     |
| iconic      |                                                                            | boolean        | -      | false       |
