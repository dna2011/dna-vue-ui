# Spin

> Component for showing loading state

<component-demo
    props-stringified="[{%042name%042:%042color%042,%042type%042:{%042name%042:%042string%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042primary%042},%042values%042:[%042primary%042,%042secondary%042,%042success%042,%042info%042,%042warning%042,%042danger%042]},{%042name%042:%042size%042,%042type%042:{%042name%042:%042string%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042sm%042},%042values%042:[%042sm%042,%042lg%042,%042xl%042]}]"
    slots-stringified="[{%042name%042:%042default%042}]"
    component-name="dv-spin">
</component-demo>

---

## Props

| Prop name | Description | Type   | Values                                                         | Default   |
| --------- | ----------- | ------ | -------------------------------------------------------------- | --------- |
| color     |             | string | `primary`, `secondary`, `success`, `info`, `warning`, `danger` | 'primary' |
| size      |             | string | `sm`, `lg`, `xl`                                               | 'sm'      |

## Slots

| Name    | Description | Bindings |
| ------- | ----------- | -------- |
| default |             |          |
