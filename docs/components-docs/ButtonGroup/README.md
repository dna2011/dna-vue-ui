# Button Group

> Component for grouping button in different cases

<component-demo
    props-stringified="[{%042name%042:%042indent%042,%042description%042:%042Sets distance between buttons in group%042,%042type%042:{%042name%042:%042string%042},%042defaultValue%042:{%042func%042:false,%042value%042:%0420%042},%042values%042:[%0420%042,%0421%042,%0422%042,%0423%042]}]"
    slots-stringified="[{%042name%042:%042default%042,%042description%042:%042Place DVButton here%042,%042bindings%042:[]}]"
    component-name="dv-button-group">
</component-demo>

---

## Props

| Prop name | Description                            | Type   | Values             | Default |
| --------- | -------------------------------------- | ------ | ------------------ | ------- |
| indent    | Sets distance between buttons in group | string | `0`, `1`, `2`, `3` | '0'     |

## Slots

| Name    | Description         | Bindings |
| ------- | ------------------- | -------- |
| default | Place DVButton here |          |
