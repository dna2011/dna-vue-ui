# Button

> Button component. Allows several customization options;

<component-demo
    props-stringified="[{%042name%042:%042pending%042,%042description%042:%042Makes button disabled, shows DVSpin%042,%042type%042:{%042name%042:%042boolean%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042false%042}},{%042name%042:%042href%042,%042description%042:%042Creates link tag with this href%042,%042type%042:{%042name%042:%042string%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042null%042}},{%042name%042:%042size%042,%042type%042:{%042name%042:%042string%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042normal%042},%042values%042:[%042small%042,%042normal%042,%042large%042]},{%042name%042:%042color%042,%042description%042:%042Sets background color for button according to theme%042,%042type%042:{%042name%042:%042string%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042basic%042},%042values%042:[%042basic%042,%042secondary%042,%042success%042,%042info%042,%042warning%042,%042danger%042]},{%042name%042:%042dense%042,%042description%042:%042Reduces vertical paddings%042,%042type%042:{%042name%042:%042boolean%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042false%042}},{%042name%042:%042rounded%042,%042description%042:%042Adds border radius%042,%042type%042:{%042name%042:%042boolean%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042false%042}},{%042name%042:%042text%042,%042description%042:%042Draw button with transparent background/border%042,%042type%042:{%042name%042:%042boolean%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042false%042}},{%042name%042:%042outlined%042,%042description%042:%042Draw button with transparent background%042,%042type%042:{%042name%042:%042boolean%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042false%042}},{%042name%042:%042iconic%042,%042description%042:%042Draw icon style button%042,%042type%042:{%042name%042:%042boolean%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042false%042}}]"
    slots-stringified="[{%042name%042:%042icon%042,%042description%042:%042Use DVIcon componenet here%042},{%042name%042:%042default%042,%042description%042:%042Content slot%042,%042tags%042:{%042demoContent%042:[{%042description%042:%042Click me!%042,%042title%042:%042demoContent%042}]}}]"
    component-name="dv-button">
</component-demo>

---

## Props

| Prop name | Description                                         | Type    | Values                                                       | Default  |
| --------- | --------------------------------------------------- | ------- | ------------------------------------------------------------ | -------- |
| pending   | Makes button disabled, shows DVSpin                 | boolean | -                                                            | false    |
| href      | Creates link tag with this href                     | string  | -                                                            | null     |
| size      |                                                     | string  | `small`, `normal`, `large`                                   | 'normal' |
| color     | Sets background color for button according to theme | string  | `basic`, `secondary`, `success`, `info`, `warning`, `danger` | 'basic'  |
| dense     | Reduces vertical paddings                           | boolean | -                                                            | false    |
| rounded   | Adds border radius                                  | boolean | -                                                            | false    |
| text      | Draw button with transparent background/border      | boolean | -                                                            | false    |
| outlined  | Draw button with transparent background             | boolean | -                                                            | false    |
| iconic    | Draw icon style button                              | boolean | -                                                            | false    |

## Slots

| Name    | Description                | Bindings |
| ------- | -------------------------- | -------- |
| icon    | Use DVIcon componenet here |          |
| default | Content slot               |          |
