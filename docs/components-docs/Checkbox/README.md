# Checkbox

<component-demo
    props-stringified="[{%042name%042:%042v-model%042,%042description%042:%042Stores checkbox/checkbox-group state.%042,%042tags%042:{%042model%042:[{%042description%042:true,%042title%042:%042model%042}]},%042type%042:{%042name%042:%042array|boolean%042},%042required%042:true},{%042name%042:%042option%042,%042description%042:%042Specify checkbox value. Use it when checkbox group is presented.%042,%042type%042:{%042name%042:%042string%042}},{%042name%042:%042disabled%042,%042type%042:{%042name%042:%042boolean%042},%042defaultValue%042:{%042func%042:false,%042value%042:%042false%042}}]"
    slots-stringified="[{%042name%042:%042default%042,%042description%042:%042Checkbox label%042,%042tags%042:{%042demoContent%042:[{%042description%042:%042Option%042,%042title%042:%042demoContent%042}]}}]"
    component-name="dv-checkbox">
</component-demo>

---

## Props

| Prop name | Description                                                      | Type           | Values | Default |
| --------- | ---------------------------------------------------------------- | -------------- | ------ | ------- |
| v-model   | Stores checkbox/checkbox-group state.                            | array\|boolean | -      |         |
| option    | Specify checkbox value. Use it when checkbox group is presented. | string         | -      |         |
| disabled  |                                                                  | boolean        | -      | false   |

## Slots

| Name    | Description    | Bindings |
| ------- | -------------- | -------- |
| default | Checkbox label |          |
