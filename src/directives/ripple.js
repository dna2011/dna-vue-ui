// TODO: refact: use closure for store $ripple etc. if possible
import DomHandler from '../utils/DomHandler';
import './ripple.scss';

const RIPPLE_CLASS = 'dv-ripple';
const RIPPLE_ACTIVE_CLASS = 'dv-ripple--active';

function onAnimationEnd(event) {
  DomHandler.removeClass(event.currentTarget, RIPPLE_ACTIVE_CLASS);
}

function createRippleEl(el) {
  const $ripple = document.createElement('span');
  $ripple.className = RIPPLE_CLASS;
  el.appendChild($ripple);
  $ripple.addEventListener('animationend', onAnimationEnd);
}

function onMouseDown(event) {
  const target = event.currentTarget;
  const $ripple = DomHandler.getChildElByClassname(target, RIPPLE_CLASS);

  if (!$ripple || getComputedStyle($ripple).display === 'none') {
    return;
  }

  DomHandler.removeClass($ripple, RIPPLE_ACTIVE_CLASS);

  if (!DomHandler.getElWidth($ripple) && !DomHandler.getElHeight($ripple)) {
    const maxSize = Math.max(
      DomHandler.getElHeight(target),
      DomHandler.getElWidth(target),
    );
    $ripple.style.height = `${maxSize}px`;
    $ripple.style.width = `${maxSize}px`;
  }

  const targetOffset = DomHandler.getOffset(target);
  const x = event.pageX - targetOffset.left - DomHandler.getElWidth($ripple) * 0.5;
  const y = event.pageY - targetOffset.top - DomHandler.getElHeight($ripple) * 0.5;

  $ripple.style.top = `${y}px`;
  $ripple.style.left = `${x}px`;
  DomHandler.addClass($ripple, RIPPLE_ACTIVE_CLASS);
}

function remove(el) {
  const $ripple = DomHandler.getChildElByClassname(el, RIPPLE_CLASS);
  if ($ripple) {
    el.removeEventListener('mousedown', onMouseDown);
    $ripple.removeEventListener('animationend', onAnimationEnd);
    $ripple.remove();
  }
}

export default {
  inserted(el) {
    createRippleEl(el);
    el.addEventListener('mousedown', onMouseDown);
  },

  unbind(el) {
    remove(el);
  },
};
