export default class DomHandler {
  static getChildElByClassname(el, cssClass) {
    for (let i = 0; i < el.children.length; i += 1) {
      if (el.children[i].className.indexOf(cssClass) !== -1) {
        return el.children[i];
      }
    }
    return null;
  }

  static getElHeight(el) {
    return el.offsetHeight;
  }

  static getElWidth(el) {
    return el.offsetWidth;
  }

  static removeClass(el, cssClass) {
    el.classList.remove(cssClass);
  }

  static addClass(el, cssClass) {
    el.classList.add(cssClass);
  }

  static getOffset(el) {
    const rect = el.getBoundingClientRect();

    return {
      top: rect.top + (window.pageYOffset
        || document.documentElement.scrollTop
        || document.body.scrollTop
        || 0),
      left: rect.left + (window.pageXOffset
        || document.documentElement.scrollLeft
        || document.body.scrollLeft
        || 0),
    };
  }
}
