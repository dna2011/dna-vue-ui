import Vue from 'vue';
import DVPlugin from './components';

import App from './App.vue';

Vue.use(DVPlugin);
Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount('#app');
