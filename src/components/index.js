// TODO: impl separate components export
import '@/assets/styles/app.scss';
import '@mdi/font/scss/materialdesignicons.scss';

import Button from './Button/Button.vue';
import ButtonGroup from './ButtonGroup/ButtonGroup';
import Checkbox from './Checkbox/Checkbox.vue';
import Icon from './Icon/Icon.vue';
import List from './List/List.vue';
import Positionable from './Positionable/Positionable.js';
import SelectButton from './SelectButton/SelectButton.vue';
import Spin from './Spin/Spin.vue';
import Switch from './Switch/Switch.vue';
import ToggleButton from './ToggleButton/ToggleButton.vue';

export default {
  install(Vue) {
    Vue.component('dv-button', Button);
    Vue.component('dv-button-group', ButtonGroup);
    Vue.component('dv-checkbox', Checkbox);
    Vue.component('dv-icon', Icon);
    Vue.component('dv-list', List);
    Vue.component('dv-positionable', Positionable);
    Vue.component('dv-select-button', SelectButton);
    Vue.component('dv-spin', Spin);
    Vue.component('dv-switch', Switch);
    Vue.component('dv-toggle-button', ToggleButton);
  },
};
