import './ButtonGroup.scss';

/**
 * Component for grouping button in different cases
 * @displayName Button Group
 */

export default {
  name: 'dv-button-group',

  props: {
    /**
     * Sets distance between buttons in group
     */
    indent: {
      type: String,
      default: '0',
      validator: (indent) => ['0', '1', '2', '3'].includes(indent),
    },
  },

  /**
   * Place DVButton here
   * @slot default
   */

  render(h) {
    this.$slots.default.forEach((c) => {
      if (!c.componentOptions || c.componentOptions.tag !== 'dv-button') {
        console.warn('dv-button-group can only process dv-button component');
      }
    });

    return h('div',
      {
        staticClass: 'dv-button-group',
        class: `dv-button-group--indent-${this.indent}`,
      }, this.$slots.default);
  },
};
