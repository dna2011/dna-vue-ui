import { createPopper } from "@popperjs/core";
import "./Positionable.scss";

export default {
  name: "positionable",

  props: {
    anchor: {
      type: HTMLElement,
      default: null
    },

    visible: {
      type: Boolean
    }
  },

  data: () => ({}),

  watch: {
    anchor() {
      this.$nextTick(() => {
        createPopper(this.anchor, this.$el, {
          placement: "bottom"
        });
      });
    }
  },

  render(h) {
    return h(
      "transition",
      {
        props: {
          appear: true,
          name: "fade"
        }
      },
      [
        h(
          "div",
          { class: "positionable", directives: [{ name: "show", value: this.visible }] },
          this.$slots.default
        )
      ]
    );
  }
};
