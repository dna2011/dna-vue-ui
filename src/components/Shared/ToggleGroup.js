// TODO: add mandatory process in created hook

export default {
  name: 'toggle-group',

  props: {
    value: {
      type: [Array, Boolean, String],
      required: true
    },

    disabled: {
      type: Boolean,
      default: false
    },

    tag: {
      type: String,
      default: 'div'
    }
  },

  methods: {
    onInput(val) {
      let payload;
      if (Array.isArray(this.value)) {
        if (this.value.includes(val)) {
          if (this.mandatory && this.value.length === 1) {
            payload = this.value;
          } else {
            payload = this.value.filter((v) => v !== val);
          }
        } else {
          payload = [...this.value, val];
        }
      } else if (this.value === val) {
        payload = this.mandatory ? val : undefined;
      } else {
        payload = val;
      }

      this.$emit("input", payload);
    }
  },

  render(h) {
    return h(this.tag, this.$scopedSlots.default({ onInput: this.onInput, value: this.value }));
  }
}